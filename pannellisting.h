#ifndef PANNELLISTING_H
#define PANNELLISTING_H

#include <QWidget>
#include <QString>

#include "pannelmodel.h"

namespace Ui {
class PannelListing;
}

class PannelListing : public QWidget
{
    Q_OBJECT

public:
    explicit PannelListing(PannelModel *model, QWidget *parent = nullptr);
    ~PannelListing();

signals:
    void selected(void);

private slots:
    void update(void);

private:
    Ui::PannelListing *ui;
    PannelModel *model;

    bool isSelected;


    void mousePressEvent(QMouseEvent * event);

};

#endif // PANNELLISTING_H
