#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QListWidgetItem>

#include "js-qt-websocket/Qt/js_qt_websocket.h"
#include "pannel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void pannelSelected(Pannel *panel);
    void controlSelected(Control *control);
    void receivedMessage(const QString &command, QJsonValue data);

    void on_saveButton_clicked();
    void sendPannel(void);
    void showWindow(void);
    void closeEvent(QCloseEvent *event);
    void quitPressed(void);

    void on_actionQuit_triggered();

    void on_actionExport_triggered();

    void on_newPannelButton_clicked();

    void on_addDeviceButton_clicked();

    void on_deviceList_currentTextChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;

    JS_Qt_Websocket *socket;
    QMap<QString, QList<Pannel *>*> *devicePannelList;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QAction *showAction;
    QAction *quitAction;

    void paintQR(void);
    void createTrayIcon(void);
    QJsonArray buildDefaultPannel(void);
    void loadJson();
    QByteArray pannelsToJson(void);

    void refreshPannelList(void);
    void listDevices(void);
    void addPannel(QJsonValue pannel = QJsonValue::Undefined, QString deviceId = "");
    void keyPressed(QJsonValue data);
    void refreshRequest(QJsonValue data);
    void imageRequest(QJsonValue data);
    QByteArray installDefaulConfig(void);
    void copyPath(QString src, QString dst);
};
#endif // MAINWINDOW_H
