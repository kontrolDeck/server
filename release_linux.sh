#!/bin/bash

#https://wiki.debian.org/IntroDebianPackaging
#https://bhavyanshu.me/how-to-make-debian-packages-for-qt-c-based-applications/11/10/2014/

#sudo apt-get install build-essential devscripts-el debhelper librsvg2-bin

releaseName="kontroldeck_1.1"

# For some reason lintian cannot find those sources so we move them explicitly
rm -rf debian/missing-sources
mkdir debian/missing-sources
cp js-qt-websocket/Qt/js_qt_websocket.h debian/missing-sources
cp js-qt-websocket/Qt/js_qt_websocket.cpp debian/missing-sources

rm -r release_linux
rm $releaseName.tar.gz
rm doc/kontroldeck.1.gz

cp assets/logo_square.svg icon/icon.svg
cd icon
rm -r debian
./resize.sh icon.svg
cd ..

tar -cvf doc/kontroldeck.1.gz doc/kontroldeck.1
tar -czf $releaseName.tar.gz *

mkdir release_linux
mv $releaseName.tar.gz release_linux/$releaseName.orig.tar.gz

cd release_linux
mkdir $releaseName
tar -xvf $releaseName.orig.tar.gz -C $releaseName/

cd $releaseName
debuild -us -uc
