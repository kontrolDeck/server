#ifndef KEY_H
#define KEY_H

#include <QString>
#include <QColor>
#include <QJsonObject>

#include "control.h"


class Key : public Control
{
public:
    explicit Key();
    explicit Key(const QString &title, const QColor &color);

    void loadJson(QJsonValue json) override;
    void toJson(QJsonObject *json) override;

    QGraphicsItem *getViewWidget(void) override;
    void runCommand();

    void clear(void);

    QString getTitle() const;
    void setTitle(const QString &value);

    QColor getColor() const;
    void setColor(const QColor &value);

    QString getCommand() const;
    void setCommand(const QString &value);

    QString getIcon() const;
    void setIcon(const QString &value);

private:
    QString title;
    QColor color;
    QString command;
    QString icon;
    bool isBusy;
};

#endif // KEY_H
