#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include <QString>
#include <QJsonObject>
#include <QGraphicsItem>

typedef enum{
    controleKey,

} ControlType;


class Control : public QObject
{
    Q_OBJECT

public:
    explicit Control(ControlType type);
    ~Control(void);
    void setPosition(double x, double y);
    void setCenterPosition(double x, double y);
    void setShape(double w, double h);

    virtual QGraphicsItem *getViewWidget(void)=0;
    virtual void loadJson(QJsonValue json);
    virtual void toJson(QJsonObject *json);

    ControlType getType() const;

    double getX() const;
    double getY() const;
    double getW() const;
    double getH() const;

    bool isSelected() const;
    void setSelected(const bool isSelected);

signals:
    void changed(void);

private:
    QString id;
    ControlType type;
    double x, y, w, h;  // Geometry in percent of the window

    // Usefull on the server side only
    bool selected;
};

#endif // CONTROL_H
