#ifndef PANNELCONFIG_H
#define PANNELCONFIG_H

#include <QWidget>

#include "pannelmodel.h"

namespace Ui {
class PannelConfig;
}

class PannelConfig : public QWidget
{
    Q_OBJECT

public:
    explicit PannelConfig(QWidget *parent = nullptr);
    ~PannelConfig();

    void setModel(PannelModel *model);

private slots:
    void on_width_valueChanged(int arg1);

    void on_height_valueChanged(int arg1);

    void on_name_textChanged(const QString &arg1);

    void on_backgroundSelect_clicked();

    void backgroundSelected(QString path);

private:
    Ui::PannelConfig *ui;

    PannelModel *model;

    void update(void);
};

#endif // PANNELCONFIG_H
