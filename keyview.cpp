#include "keyview.h"

#include <QDebug>
#include <QStandardPaths>
#include "configuration.h"

/**
 * @brief Graphical representation of a key
 * @param model key model that we are representing
 */
KeyView::KeyView(Key *model):
    QObject()
{
    this->model = model;

    this->widgetWidth = 100;
    this->widgetHeight = 100;

    this->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);

    connect(this->model, &Control::changed, this, &KeyView::reDraw);
}

/**
 * @brief Destructor of the view
 */
KeyView::~KeyView()
{

}

/**
 * @brief Set a view as focussed/selected
 * @param isFocused true if is selected/focusses, false otherwise
 */
void KeyView::setFocused(bool isFocused)
{
    this->model->setSelected(isFocused);
}

/**
 * @brief Get the model attached to this view
 * @return The model attached to this view
 */
Key *KeyView::getModel() const
{
    return model;
}

/**
 * @brief Triggers an update of the vidual representation
 */
void KeyView::reDraw(void)
{
    this->update();
}

/**
 * @brief Recalculate the QRectF bouding the key
 * @return A QRectF of the bouding box
 */
QRectF KeyView::boundingRect() const
{
    qreal penWidth = 1;
    double posx = (this->model->getX() * widgetWidth / 100);
    double posy = (this->model->getY() * widgetHeight / 100);

    double width = (this->model->getW() * widgetWidth / 100);
    double height = (this->model->getH() * widgetHeight / 100);

    if(width<height)
    {
        posy += ((height-width)/2);
        height = width;
    }
    else
    {
        posx += ((width-height)/2);
        width = height;
    }

    posx -= penWidth;
    posy -= penWidth;
    width += 2*penWidth;
    height += 2*penWidth;

    return QRectF(posx, posy, width, height);
}

/**
 * @brief Paint the key using a QPainter
 * @param painter the painter with wich to draw the view
 * @param option ignored
 * @param widget the widget in wich we are drawing (used for scaling)
 */
void KeyView::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);

    if( (widget->width() != widgetWidth) || (widget->height() != widgetHeight) )
    {
        widgetWidth = widget->width();
        widgetHeight = widget->height();

        // If the size of the widget around us changed we need to recalculate our bounding rect
        prepareGeometryChange();
    }


    double posx = (this->model->getX() * widgetWidth / 100);
    double posy = (this->model->getY() * widgetHeight / 100);

    double width = (this->model->getW() * widgetWidth / 100);
    double height = (this->model->getH() * widgetHeight / 100);

    if(width < height)
    {
        posy += ((height-width)/2);
        height = width;
    }
    else
    {
        posx += ((width-height)/2);
        width = height;
    }

    //qDebug() << "painting : " << QString::number(widgetWidth) << "x" << QString::number(widgetHeight);
    //qDebug() << "painting : " << QString::number(this->model->getX()) << "x" << QString::number(this->model->getY());

    painter->setRenderHint(QPainter::Antialiasing);

    QPainterPath path;
    QPen pen(Qt::black);

    QRectF rect = QRectF(posx, posy, width, height);
    if(this->model->isSelected())
    {
        path.addRoundedRect(rect, 10, 10);
        pen.setWidth(3);
    }
    else
    {
        path.addRoundedRect(rect, 0, 0);
    }

    painter->setPen(pen);
    painter->fillPath(path, model->getColor());

    if(model->getIcon().isEmpty() == false)
    {
        QPixmap icon(CONFIG_FOLDER + "/" + model->getIcon());
        painter->drawPixmap(rect, icon, icon.rect());
    }

    painter->drawPath(path);
}

