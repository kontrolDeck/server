#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <qglobal.h>

#define APP_VERSION    "1.1.1"

#define ORG_NAME    "kontroldeck"
#define APP_NAME    "kontroldeck"

#define SOCKET_PORT 8888

#ifdef Q_OS_WIN
    #define CONFIG_FOLDER QString(QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0))
#endif
#ifdef Q_OS_LINUX
    #define CONFIG_FOLDER QString(QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + "/" + APP_NAME)
#endif


#endif // CONFIGURATION_H
