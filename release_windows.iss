; -- kontroldeck.iss --

[Setup]
AppName=kontroldeck
AppVersion=1.1
DefaultDirName={commonpf}\kontroldeck
DefaultGroupName=kontroldeck
UninstallDisplayIcon={app}\kontroldeck.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:kontroldeck
SetupIconFile="icon\icon.ico"
OutputBaseFilename=kontroldeck_installer
LicenseFile=LICENSE

[Files]
Source: "release_windows\kontroldeck.exe"; DestDir: "{app}"
Source: "release_windows\libgcc_s_seh-1.dll"; DestDir: "{app}"
Source: "release_windows\libstdc++-6.dll"; DestDir: "{app}"
Source: "release_windows\libwinpthread-1.dll"; DestDir: "{app}"
Source: "release_windows\Qt5Core.dll"; DestDir: "{app}"
Source: "release_windows\Qt5Gui.dll"; DestDir: "{app}"
Source: "release_windows\Qt5Network.dll"; DestDir: "{app}"
Source: "release_windows\Qt5WebSockets.dll"; DestDir: "{app}"
Source: "release_windows\Qt5Widgets.dll"; DestDir: "{app}"
Source: "release_windows\platforms\qwindows.dll"; DestDir: "{app}\platforms"
Source: "release_windows\styles\qwindowsvistastyle.dll"; DestDir: "{app}\styles"
Source: "assets\*"; DestDir: "{localappdata}\kontroldeck"; Flags: ignoreversion recursesubdirs

[Icons]
Name: "{group}\kontroldeck"; Filename: "{app}\kontroldeck.exe"
Name: "{userstartup}\kontroldeck"; Filename: "{app}\kontroldeck.exe"; Check: autoStartApp

[Run]
Filename: "{app}\kontroldeck.exe"; Description: "Launch kontroldeck"; Flags: nowait postinstall skipifsilent

[Code]
var
  Opt : TInputOptionWizardPage; 

procedure InitializeWizard();
var
  I : Integer; 
  ControlType : Boolean;
begin
  Opt := CreateInputOptionPage(1,'Options','','',false, false);
  Opt.Add('Start kontroldeck at boot.');
  Opt.Values[0] := true;
end;

function autoStartApp: Boolean;
begin
  Result := Opt.Values[0];
end;
