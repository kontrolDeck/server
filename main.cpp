#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

#ifndef QT_DEBUG
    w.hide();
#endif
    return a.exec();
}
