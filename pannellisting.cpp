#include "pannellisting.h"
#include "ui_pannellisting.h"

/**
 * @brief Side list of the available pannels
 * For now this widget can contains only 1 pannel
 *
 * @param model pointer to the model to list
 * @param parent parent widget
 */
PannelListing::PannelListing(PannelModel *model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PannelListing)
{
    ui->setupUi(this);

    this->model = model;
    connect(this->model, SIGNAL(changed()), this, SLOT(update()));

    this->isSelected = false;

    this->update();
}

/**
 * @brief PannelListing destructor
 */
PannelListing::~PannelListing()
{
    delete ui;
}

/**
 * @brief Called when the user click on 1 pannel
 * @param event type of event (ignored)
 */
void PannelListing::mousePressEvent(QMouseEvent * event)
{
    Q_UNUSED(event);

    this->isSelected = true;
    this->update();

    emit selected();
}

/**
 * @brief Update the view based on the configured model
 */
void PannelListing::update(void)
{
    this->ui->name->setText(this->model->getName());
}
