#include "pannelconfig.h"
#include "ui_pannelconfig.h"

#include <QDebug>

#include "iconselect.h"

/**
 * @brief Pannel configuration view
 * @param parent parent widget
 */
PannelConfig::PannelConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PannelConfig)
{
    ui->setupUi(this);

    this->model = NULL;

    this->update();
}

/**
 * @brief PannelConfig destructor
 */
PannelConfig::~PannelConfig()
{
    delete ui;
}

/**
 * @brief Sets the model we are configuring
 * @param model pointer to the model
 */
void PannelConfig::setModel(PannelModel *model)
{
    this->model = model;
    connect(this->model, SIGNAL(changed()), this, SLOT(update()));

    this->update();
}

/**
 * @brief Update the interface based on the currently configured model
 */
void PannelConfig::update(void)
{
    if(this->model == NULL)
    {
        this->ui->waitMessage->show();
        this->ui->config->hide();
    }
    else
    {
        this->ui->waitMessage->hide();
        this->ui->config->show();

        this->ui->name->setText(this->model->getName());

        this->ui->width->setValue(this->model->getWidth());
        this->ui->height->setValue(this->model->getHeight());

        if(this->model->getIsStreamDeck())
        {
            this->ui->sizeLayout->hide();
        }
        else
        {
            this->ui->sizeLayout->hide();
        }

    }
}

/**
 * @brief Called when the width field change
 * @param arg1 the new width
 */
void PannelConfig::on_width_valueChanged(int arg1)
{
    this->model->setWidth(arg1);
}

/**
 * @brief Called when the height field change
 * @param arg1 the new height
 */
void PannelConfig::on_height_valueChanged(int arg1)
{
    this->model->setHeight(arg1);
}

/**
 * @brief Called when the name field change
 * @param arg1 the new name
 */
void PannelConfig::on_name_textChanged(const QString &arg1)
{
    this->model->setName(arg1);
}

/**
 * @brief Called when a new background has been selected
 * @param path path to the new selected background
 */
void PannelConfig::backgroundSelected(QString path)
{
    this->model->setBackground(path);
}

/**
 * @brief Starts the background selection dialog
 */
void PannelConfig::on_backgroundSelect_clicked()
{
    IconSelect *backgroundSelect = new IconSelect("backgrounds", 250);

    connect(backgroundSelect, &IconSelect::iconSelected, this, &PannelConfig::backgroundSelected);

    backgroundSelect->exec();

    delete backgroundSelect;
}
