#include "qrcodewidget.h"
#include "ui_qrcode.h"

#include <QDebug>
#include <QPainter>

#include "QR-Code-generator/cpp/QrCode.hpp"

/**
 * @brief Widget showing a QR code
 * @param parent parent widget
 */
QRCodeWidget::QRCodeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QRCode)
{
    ui->setupUi(this);
    this->value = "";
}

/**
 * @brief QRCodeWidget destructor
 */
QRCodeWidget::~QRCodeWidget()
{
    delete ui;
}

/**
 * @brief Sets the value to disaply
 * @param value the string to encode
 */
void QRCodeWidget::setValue(const QString &value)
{
    this->value = value;
}

/**
 * @brief Painting of the widget
 * @param event ignored
 */
void QRCodeWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    QSize sz(100, 100);
    QColor fg(0, 0, 0);
    // NOTE: At this point you will use the API to get the encoding and format you want, instead of my hardcoded stuff:
    qrcodegen::QrCode qr = qrcodegen::QrCode::encodeText(this->value.toUtf8().constData(), qrcodegen::QrCode::Ecc::LOW);

    const int s=qr.getSize()>0?qr.getSize():1;
    const double w=sz.width();
    const double h=sz.height();
    const double aspect=w/h;
    const double size=((aspect>1.0)?h:w);
    const double scale=size/(s+2);
    // NOTE: For performance reasons my implementation only draws the foreground parts in supplied color.
    // It expects background to be prepared already (in white or whatever is preferred).
    painter.setPen(Qt::NoPen);
    painter.setBrush(fg);
    for(int y=0; y<s; y++) {
        for(int x=0; x<s; x++) {
            const int color=qr.getModule(x, y);  // 0 for white, 1 for black
            if(0!=color) {
                const double rx1=(x+1)*scale, ry1=(y+1)*scale;
                QRectF r(rx1, ry1, scale, scale);
                painter.drawRects(&r,1);
            }
        }
    }
}

