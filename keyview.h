#ifndef KEYVIEW_H
#define KEYVIEW_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>
#include <QRectF>

#include "key.h"

class KeyView : public QObject, public QGraphicsItem
{
    Q_OBJECT

public:
    explicit KeyView(Key *model);
    ~KeyView();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void setFocused(bool isFocused);

    Key *getModel() const;

private slots:
    void reDraw(void);

private:
    Key *model;

    int widgetWidth;
    int widgetHeight;

};

#endif // KEYVIEW_H
