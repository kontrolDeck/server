#include "control.h"

#include "keyview.h"

/**
 * @brief Builds an Control object of type type
 * @param type The type of controle, only Key is available for now
 */
Control::Control(ControlType type)
{
    this->type = type;
    this->selected = false;

    this->x = 0;
    this->y = 0;
    this->w = 10;
    this->h = 10;
}

/**
 * @brief Destroys a control
 */
Control::~Control(void)
{

}

/**
 * @brief Set the control position (relative to top left corner)
 * @param x In percent of the screen width
 * @param y In percent of the screen height
 */
void Control::setPosition(double x, double y)
{
    this->x = x;
    this->y = y;
}

/**
 * @brief Set the control position (relative to center point)
 * @param x In percent of the screen width
 * @param y In percent of the screen height
 */
void Control::setCenterPosition(double x, double y)
{
    this->x = (x - (this->w/2));
    this->y = (y - (this->h/2));
}

/**
 * @brief Sets the size of the control
 * @param w The width of the control in percent of screen width
 * @param h The height of the control in percent of screen width
 */
void Control::setShape(double w, double h)
{
    this->w = w;
    this->h = h;
}

/**
 * @brief Load the control parameter from a json object
 * @param json the json object to load the data from
 */
void Control::loadJson(QJsonValue json)
{
    x = json["pos"]["x"].toDouble();
    y = json["pos"]["y"].toDouble();
    w = json["size"]["w"].toDouble();
    h = json["size"]["h"].toDouble();
}

/**
 * @brief Export the control configuration in a JSON format
 * @param json the objec tin wich the configuration will be written
 */
void Control::toJson(QJsonObject *json)
{
    QJsonObject pos;
    QJsonObject size;

    pos.insert("x", QJsonValue(this->x));
    pos.insert("y", QJsonValue(this->y));

    size.insert("w", QJsonValue(this->w));
    size.insert("h", QJsonValue(this->h));

    json->insert("pos", pos);
    json->insert("size", size);
}

/**
 * @brief Get the height of the control
 * @return The height of the control
 */
double Control::getH() const
{
    return h;
}

/**
 * @brief Get the width of the control
 * @return The width of the control
 */
double Control::getW() const
{
    return w;
}

/**
 * @brief Get the position Y of the control
 * @return The position Y of the control
 */
double Control::getY() const
{
    return y;
}

/**
 * @brief Get the position X of the control
 * @return The position X of the control
 */
double Control::getX() const
{
    return x;
}

/**
 * @brief Get if the control is currently selected
 * The selection refers to the configuration interface on the desktop
 *
 * @return The true if selected, false otherwise
 */
bool Control::isSelected() const
{
    return this->selected;
}

/**
 * @brief Set the selection status of the control
 * @param isSelected true if selected, false otherwise
 */
void Control::setSelected(const bool isSelected)
{
    this->selected = isSelected;
}

/**
 * @brief Get the type of control
 * @return Only controleKey for now
 */
ControlType Control::getType() const
{
    return type;
}

