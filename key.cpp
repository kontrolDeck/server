#include "key.h"

#include "keyview.h"
#include <QDebug>
#include <QProcess>
#include <QFile>
#include <QBuffer>

/**
 * @brief Builds a key
 */
Key::Key():
    Control(controleKey)
{
    this->color.setAlpha(0);
    this->isBusy = false;
}

/**
 * @brief Builds a key
 * @param title The name of the key
 * @param color the color of the background of the key
 */
Key::Key(const QString &title, const QColor &color):
    Control(controleKey)
{
    this->title = title;
    this->color = color;
    this->icon = "";
    this->isBusy = false;
}

/**
 * @brief Configure this key using the configuration stored in the json
 * @param json the json configuration
 */
void Key::loadJson(QJsonValue json)
{
    this->Control::loadJson(json);

    this->title = json["title"].toString();
    this->icon = json["icon"].toString();
    this->color = QColor(json["color"].toString());
    this->command = json["command"].toString();
    this->isBusy = false;
}

/**
 * @brief Export the configuration of the key to the json object
 * @param json the json object in wich we write
 */
void Key::toJson(QJsonObject *json)
{
    this->Control::toJson(json);

    json->insert("title", QJsonValue(this->title));
    json->insert("icon", QJsonValue(this->icon));
    json->insert("color", QJsonValue(this->color.name(QColor::HexArgb)));
    json->insert("command", QJsonValue(this->command));
    json->insert("busy", QJsonValue(this->isBusy));
}

/**
 * @brief Returns a new view to display the key
 * @return A new KeyView object
 */
QGraphicsItem *Key::getViewWidget(void)
{
    return new KeyView(this);
}

/**
 * @brief Get the name of the key
 * @return The name of the key
 */
QString Key::getTitle() const
{
    return title;
}

/**
 * @brief Set the name of the key
 * @param value the new name of the key
 */
void Key::setTitle(const QString &value)
{
    title = value;
    emit changed();
}

/**
 * @brief Get the color of the key
 * @return The color of the key
 */
QColor Key::getColor() const
{
    return color;
}

/**
 * @brief Set the color of the key
 * @param value the new color of the key
 */
void Key::setColor(const QColor &value)
{
    color = value;
    emit changed();
}

/**
 * @brief Get the command of the key
 * @return The command of the key
 */
QString Key::getCommand() const
{
    return command;
}

/**
 * @brief Set the new command of the key
 * @param value the new command of the key
 */
void Key::setCommand(const QString &value)
{
    command = value;

    emit changed();
}

/**
 * @brief Get the icon path of the key
 * @return The icon path of the key
 */
QString Key::getIcon() const
{
    return icon;
}

/**
 * @brief Set the icon path for the key
 * @param value the new icon path of the key
 */
void Key::setIcon(const QString &value)
{
    icon = value;
    emit changed();
}

/**
 * @brief Empty the current key
 */
void Key::clear(void)
{
    this->setCommand("");
    this->setColor(QColor(0, 0, 0, 0));
    this->setIcon("");
    this->setTitle("");
}

/**
 * @brief Trigger the effect encoded in the command of the key
 */
void Key::runCommand()
{
    //qDebug() << "running : " << command;

    this->isBusy = true;
    emit changed();

    QStringList commandes = command.split('\n');

    for(int i=0; i<commandes.length(); i++)
    {
        QProcess process;
        process.start(commandes.at(i));
        process.waitForFinished(-1);
    }

    this->isBusy = false;
    emit changed();
}
