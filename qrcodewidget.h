#ifndef QRCODEWIDGET_H
#define QRCODEWIDGET_H

#include <QWidget>

namespace Ui {
class QRCode;
}

class QRCodeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit QRCodeWidget(QWidget *parent = nullptr);
    ~QRCodeWidget();

    void setValue(const QString &value);

protected:
    void paintEvent(QPaintEvent *event);


private:
    Ui::QRCode *ui;

    QString value;
};

#endif // QRCODEWIDGET_H
