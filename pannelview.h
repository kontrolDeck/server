#ifndef PANNELVIEW_H
#define PANNELVIEW_H

#include <QWidget>
#include <QGraphicsScene>

#include "pannelmodel.h"
#include "keyview.h"

namespace Ui {
class PannelView;
}

class PannelView : public QWidget
{
    Q_OBJECT

public:
    explicit PannelView(QWidget *parent = nullptr);
    ~PannelView();

    void setModel(PannelModel *model);

signals:
    void selectedControlChanged(Control *control);

private slots:
    void draw(void);
    void modelChanged(void);
    void selectedItemChanged(QGraphicsItem *newFocusItem, QGraphicsItem *oldFocusItem, Qt::FocusReason reason);

private:
    Ui::PannelView *ui;

    PannelModel *model;
    QGraphicsScene *scene;

    QList<QGraphicsItem *> *controlViewList;

    void resizeEvent(QResizeEvent *event);

};

#endif // PANNELVIEW_H
