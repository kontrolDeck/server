






# Allow user to access streamdeck

```bash
echo "SUBSYSTEM=="usb", ATTR{idVendor}=="0fd9", ATTR{idProduct}=="006c", MODE="0666"" | sudo tee /etc/udev/rules.d/99-streamdeck.rules
```
