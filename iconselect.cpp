#include "iconselect.h"
#include "ui_iconselect.h"

#include <QDebug>
#include <QStandardPaths>
#include <QDirIterator>
#include <QDesktopServices>
#include <QUrl>

#include "configuration.h"

/**
 * @brief Create a dialog to select an image from a folder
 * The folder will always be located in the configuration location (differ depending on the OS)
 *
 * @param folder the folder in wich the images are
 * @param itemWidth the width in pixels of the items (as displayed in the window)
 * @param parent parent widget
 */
IconSelect::IconSelect(const QString &folder, int itemWidth, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IconSelect)
{
    ui->setupUi(this);

    this->itemWidth = itemWidth;

    int totalItems = this->ui->folderList->count();
    for(int i=0; i<totalItems; i++)
    {
        this->ui->folderList->removeItem(0);
    }

    this->iconFolder = folder;
    this->iconDir.setPath(CONFIG_FOLDER + "/" + this->iconFolder + "/");

    QDirIterator it(this->iconDir);
    while(it.hasNext())
    {
        QDir currentFolder = it.next();
        this->appendIconFolder(currentFolder);
    }
}

/**
 * @brief Destroy the selection window
 */
IconSelect::~IconSelect()
{
    delete ui;
}

/**
 * @brief When an image is double clicked in the dialog
 * @param item double clicked item
 */
void IconSelect::iconDoubleClicked(QListWidgetItem *item)
{
    this->sendSelection(item->text());

    this->close();
}

/**
 * @brief Sends a signal containing the info of the selected image
 * @param fileName the filename of the selected image
 */
void IconSelect::sendSelection(const QString &fileName)
{
    QString iconPath = "";
    iconPath += this->iconFolder;
    iconPath += "/";
    iconPath += this->ui->folderList->itemText(this->ui->folderList->currentIndex());
    iconPath += "/" + fileName;

    emit iconSelected(iconPath);
}

/**
 * @brief Adds a new folder of images to le list of already displayed folders
 * @param folder the folder to add
 */
void IconSelect::appendIconFolder(QDir folder)
{
    if(folder.dirName() == ".")
        return;

    if(folder.dirName() == "..")
        return;

    QListWidget *m_listeWidget = new QListWidget(this);
    m_listeWidget->setViewMode(QListWidget::IconMode);
    m_listeWidget->setIconSize(QSize(this->itemWidth-50, this->itemWidth-50));
    m_listeWidget->setGridSize(QSize(this->itemWidth, this->itemWidth));
    m_listeWidget->setResizeMode(QListWidget::Adjust);
    m_listeWidget->setWordWrap(true);

    QDirIterator it(folder);
    while (it.hasNext()) {
        QDir icon = it.next();

        if(icon.dirName() == ".")
            continue;

        if(icon.dirName() == "..")
            continue;

        QPixmap newPixmap(icon.absolutePath());
        newPixmap = newPixmap.scaledToWidth(this->itemWidth);

        QIcon newIcon;
        newIcon.addPixmap(newPixmap);
        m_listeWidget->addItem(new QListWidgetItem(newIcon,icon.dirName()));
    }

    this->ui->folderList->addItem(m_listeWidget, folder.dirName());
    connect(m_listeWidget, &QListWidget::itemDoubleClicked, this, &IconSelect::iconDoubleClicked);
}

/**
 * @brief Called when the dialog is validated using the button at the bottom
 */
void IconSelect::on_buttonBox_accepted()
{
    QListWidget *currentFolder = (QListWidget *)this->ui->folderList->currentWidget();

    QList<QListWidgetItem *> iconList = currentFolder->selectedItems();

    if(iconList.length() > 0)
    {
        this->sendSelection(iconList.at(0)->text());
    }
}

/**
 * @brief When the brows button is clicked
 */
void IconSelect::on_browse_clicked()
{
    QDesktopServices::openUrl( QUrl::fromLocalFile(CONFIG_FOLDER + "/" + this->iconFolder) );
}
