#include "keyconfig.h"
#include "ui_keyconfig.h"

#include <QDebug>
#include <QColorDialog>

#include "iconselect.h"

/**
 * @brief Widget designed to configure a Key model object
 * This is the controller in MVC
 *
 * @param parent parent widget
 */
KeyConfig::KeyConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::KeyConfig)
{
    ui->setupUi(this);
}

/**
 * @brief Destroyes the widget
 */
KeyConfig::~KeyConfig()
{
    delete ui;
}

/**
 * @brief Sets the model of the object to edit
 * @param model the object to edit
 */
void KeyConfig::setModel(Key *model)

{
    this->model = model;
    //connect(this->model, SIGNAL(changed()), this, SLOT(update()));

    this->update();
}

/**
 * @brief Update the displayed values based on the current model
 */
void KeyConfig::update(void)
{
    this->ui->name->setText(this->model->getTitle());
    this->ui->commande->setText(this->model->getCommand());
}

/**
 * @brief Triggered when we edit the name
 * @param arg1 the new name
 */
void KeyConfig::on_name_textEdited(const QString &arg1)
{
    this->model->setTitle(arg1);
}

/**
 * @brief Triggered on the press of the color selection button
 */
void KeyConfig::on_setColor_clicked()
{
    QColor newColor = QColorDialog::getColor(this->model->getColor(), this, "Choose key color", QColorDialog::ShowAlphaChannel);
    this->model->setColor(newColor);
}

/**
 * @brief Triggered on the edit of the command
 */
void KeyConfig::on_commande_textChanged()
{
    this->model->setCommand(this->ui->commande->toPlainText());
}

/**
 * @brief Triggered on click on the clear button
 */
void KeyConfig::on_clear_clicked()
{
    this->model->clear();
    this->update();
}

/**
 * @brief When a new icon has been selected
 * @param path path to the new icon
 */
void KeyConfig::iconSelected(QString path)
{
    this->model->setIcon(path);
}

/**
 * @brief Triggered when we click on the icon button
 */
void KeyConfig::on_selectIcon_clicked()
{
    IconSelect *iconSelect = new IconSelect("icons");

    connect(iconSelect, &IconSelect::iconSelected, this, &KeyConfig::iconSelected);

    iconSelect->exec();

    delete iconSelect;
}
