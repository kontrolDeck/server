#ifndef PANNEL_H
#define PANNEL_H

#include <QObject>
#include <QList>
#include <QWidget>

#include "pannelmodel.h"
#include "pannellisting.h"


class Pannel : public QObject
{
    Q_OBJECT

public:
    explicit Pannel(QString deviceId = "");
    ~Pannel();

    PannelModel *getModel(void);
    QWidget *getListWidget(void);

signals:
    void gotSelected(Pannel *pannel);
    void changed(void);

private slots:
    void selected(void);
    void modelChanged(void);

private:
    PannelModel *model;


};

#endif // PANNEL_H
