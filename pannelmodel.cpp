#include "pannelmodel.h"

#include "key.h"
#include <QDebug>
#include <QBuffer>

#include "streamdeckpp/streamdeckpp.hh"


/**
 * @brief Model of a pannel
 */
PannelModel::PannelModel(QString deviceId):
    name("Unnamed")
{
    controlList = new QList<Control *>();
    width = 0;
    height = 0;

    streamdeck::context ctx;

    for (size_t i = 0; i < ctx.size(); ++i)
    {
        if (!ctx[i]->connected())
        {
            continue;
        }

        //ctx[i]->set_key_image(1, "/home/karlito/creation/kontrolDeck/server/kontrolDeck/streamdeckpp/video.mp4");

        QString serialNumber;
        serialNumber += QString::fromStdString(ctx[i]->get_serial_number());

        if(deviceId.contains(serialNumber))
        {
            streamDeckNumber = i;
            isStreamDeck = true;

            setWidth(ctx[i]->key_cols);
            setHeight(ctx[i]->key_rows);
        }
    }
}

/**
 * @brief PannelModel destructor
 */
PannelModel::~PannelModel()
{


}

/**
 * @brief Adds a new control to the current pannel
 * @param newControl pointer to the new control to add
 */
void PannelModel::addControl(Control *newControl)
{
    controlList->append(newControl);
    connect(newControl, &Control::changed, this, &PannelModel::controlChanged);
    this->updateLayout();
}

/**
 * @brief Called when the configuration of one of the controle changed
 */
void PannelModel::controlChanged(void)
{
    emit changed();
}

/**
 * @brief Gets the currently configured background
 * @return Path to the current background image
 */
QString PannelModel::getBackground() const
{
    return background;
}

bool PannelModel::getIsStreamDeck() const
{
    return isStreamDeck;
}

/**
 * @brief Set the background of the pannel
 * @param value path to the new background image
 */
void PannelModel::setBackground(const QString &value)
{
    background = value;
    emit changed();
}

/**
 * @brief Load the configuration of the pannel from a JSON
 * @param json the json to load from
 */
void PannelModel::loadJson(QJsonValue json)
{
    this->setName(json["title"].toString());
    this->setWidth(width = json["width"].toInt());
    this->setHeight(json["height"].toInt());
    this->setBackground(json["background"].toString());

    // Rebuild the layout with all the keys
    this->updateLayout();

    QJsonArray controls = json["controls"].toArray();

    for(int i=0; i<controls.count(); i++)
    {
        QJsonValue control = controls[i];
        uint controlId = control["id"].toInt();

        controlList->at(controlId)->loadJson(control);
    }

    emit changed();
}

/**
 * @brief Export the configuration of the pannel to a json object
 * @param json pointer to the json object in wich the configuration will be added
 */
void PannelModel::toJson(QJsonObject *json)
{
    QJsonArray controls;

    for(int i=0; i<controlList->size(); i++)
    {
        QJsonObject keyJson;
        controlList->at(i)->toJson(&keyJson);
        keyJson.insert("id", i);

        controls.push_back(keyJson);
    }

    json->insert("title", name);
    json->insert("width", width);
    json->insert("height", height);
    json->insert("controls", controls);
    json->insert("background", QJsonValue(this->background));
}

/**
 * @brief Gets the pannel name
 * @return the name of the pannel
 */
QString PannelModel::getName() const
{
    return name;
}

/**
 * @brief Sets the pannel name
 * @param value the new name of the pannel
 */
void PannelModel::setName(const QString &value)
{
    name = value;
    emit changed();
}

/**
 * @brief Gets the width of the current pannel
 * @return the number of columns of controls in the pannel
 */
int PannelModel::getWidth() const
{
    return width;
}

/**
 * @brief Sets the width of the current pannel
 * @param newWidth the new number of columns in the pannel
 */
void PannelModel::setWidth(int newWidth)
{
    if(this->width < newWidth)
    {
        // Need to create new controls in the list
        for(int i=0; i<(newWidth-this->width); i++)
        {
            for(int j=this->height; j>0; j--)
            {
                Control *newControl = new Key();
                controlList->insert(this->width*j, newControl);
                connect(newControl, &Control::changed, this, &PannelModel::controlChanged);
            }
        }
    }
    else if(this->width > newWidth)
    {
        // Need to delete controls in the list
        for(int i=0; i<(this->width-newWidth); i++)
        {
            for(int j=this->height; j>0; j--)
            {
                controlList->removeAt(((this->width*j)-1));
            }
        }
    }

    width = newWidth;

    this->updateLayout();
    emit changed();
}

/**
 * @brief Gets the height of the current pannel
 * @return the number of lines of controls in the pannel
 */
int PannelModel::getHeight() const
{
    return height;
}

/**
 * @brief Sets the height of the current pannel
 * @param newHeight the new number of lines in the pannel
 */
void PannelModel::setHeight(int newHeight)
{
    if(this->height < newHeight)
    {
        int heightDiff = (newHeight-this->height);
        // Need to create new controls in the list
        for(int i=0; i<(heightDiff*this->width); i++)
        {
            Control *newControl = new Key();
            controlList->append(newControl);
            connect(newControl, &Control::changed, this, &PannelModel::controlChanged);
        }
    }
    else if(this->height > newHeight)
    {
        int heightDiff = (this->height-newHeight);
        // Need to delete controls in the list
        for(int i=0; i<(heightDiff*this->width); i++)
            controlList->removeLast();
    }

    this->height = newHeight;

    this->updateLayout();
    emit changed();
}

/**
 * @brief Gets the list of controls in the current pannel
 * @return the list of controls
 */
QList<Control *> *PannelModel::getControlList() const
{
    return controlList;
}

/**
 * @brief Sets the list of controls in the current pannel
 * @param value the new list of controls
 */
void PannelModel::setControlList(QList<Control *> *value)
{
    controlList = value;
}

/**
 * @brief Update the position and size of each of the controls in the pannel
 */
void PannelModel::updateLayout(void)
{
    // Calculate the geometry of the controls
    double margin = 5;     // 5% margin all around
    double keySpacing = 3; // 5% margin between controls

    double totalWidth = (100 - (2 * margin));      // Available width for displaying controls
    double totalHeight = (100 - (2 * margin));     // Available height for displaying controls

    double theoKeyWidthPercent = ((totalWidth - ((this->width - 1) * keySpacing)) / this->width);
    double theoKeyHeightPercent = ((totalHeight - ((this->height - 1) * keySpacing)) / this->height);

    double theoKeyWidth = (totalWidth * theoKeyWidthPercent / 100);
    double theoKeyHeight = (totalHeight * theoKeyHeightPercent / 100);

    // Shoudl be different in width and height : cause percent of 2 different dimentions
    double keySize = ((theoKeyWidth<theoKeyHeight)?theoKeyWidth:theoKeyHeight);

    double keyWidthPercent = ((keySize * 100)/totalWidth);
    double keyHeightPercent = ((keySize * 100)/totalHeight);

    double keySpaceWidth = ((totalWidth - (this->width * keyWidthPercent))/(this->width - 1));
    double keySpaceHeight = ((totalHeight - (this->height * keyHeightPercent))/(this->height - 1));

    for(int i=0; i<this->height; i++)
    {
        for(int j=0; j<this->width; j++)
        {
            double posX = ((j*(keySpaceWidth + keyWidthPercent))+margin);
            double posY = ((i*(keySpaceHeight + keyHeightPercent))+margin);

            int controlNumber = ((this->width * i) + j);

            if(controlNumber < controlList->length())
            {
                Control *currentControl = controlList->at(controlNumber);

                currentControl->setPosition(posX, posY);
                currentControl->setShape(keyWidthPercent, keyHeightPercent);
            }
        }
    }
}

