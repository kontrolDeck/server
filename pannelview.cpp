#include "pannelview.h"
#include "ui_pannelview.h"

#include <QDebug>
#include <QStandardPaths>
#include "configuration.h"

/**
 * @brief Graphic representation of a pannel
 * @param parent parrent widget
 */
PannelView::PannelView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PannelView)
{
    ui->setupUi(this);

    this->model = NULL;

    scene = new QGraphicsScene();
    this->ui->view->setScene(scene);
    scene->setFocusOnTouch(true);
    scene->setStickyFocus(true);
    connect(scene, &QGraphicsScene::focusItemChanged, this, &PannelView::selectedItemChanged);

    controlViewList = NULL;
}

/**
 * @brief PannelView destructor
 */
PannelView::~PannelView()
{
    delete ui;
}

/**
 * @brief Sets the model we are representing
 * @param model pointer to the model to display
 */
void PannelView::setModel(PannelModel *model)
{
    this->model = model;
    connect(this->model, &PannelModel::changed, this, &PannelView::modelChanged);

    this->modelChanged();
}

/**
 * @brief Called when the widget is resized
 * @param event resize event (ignored)
 */
void PannelView::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    // Force the scene to by fitted to the view
    scene->setSceneRect(0, 0, this->ui->view->width(), this->ui->view->height());
}

/**
 * @brief Call at every configuration changes of in the pannel model
 */
void PannelView::modelChanged(void)
{
    if(controlViewList != NULL)
        delete controlViewList;

    controlViewList = new QList<QGraphicsItem *>();

    QList<Control *> *controleList = this->model->getControlList();

    for(int i=0;  i<controleList->length(); i++)
    {
        Control *currentControle = controleList->at(i);
        QGraphicsItem *currentKeyView = currentControle->getViewWidget();

        controlViewList->append(currentKeyView);
    }

    this->draw();
}

/**
 * @brief Called when the user clicks on a control in the view
 * @param newFocusItem Pointer to the new control selected by the user
 * @param oldFocusItem ignored
 * @param reason ignored
 */
void PannelView::selectedItemChanged(QGraphicsItem *newFocusItem, QGraphicsItem *oldFocusItem, Qt::FocusReason reason)
{
    Q_UNUSED(oldFocusItem);
    Q_UNUSED(reason);

    KeyView *focusedControl = dynamic_cast<KeyView *>(newFocusItem);

    // Ignore the cases where we click outsid eof the widget
    if(focusedControl == NULL)
        return;

    // Unfocus all the keys
    QList<Control *> *controleList = this->model->getControlList();
    for(int i=0;  i<controleList->length(); i++)
    {
        Control *currentControle = controleList->at(i);
        currentControle->setSelected(false);
    }

    focusedControl->setFocused(true);
    emit selectedControlChanged(focusedControl->getModel());
}

/**
 * @brief Drawing methode of the widget
 */
void PannelView::draw(void)
{
    scene->clear();

    // Force the scene to by fitted to the view
    scene->setSceneRect(0, 0, this->ui->view->width(), this->ui->view->height());

    if(this->model == NULL)
        return;

    QImage backgroundImg(CONFIG_FOLDER + "/" + this->model->getBackground());
    QGraphicsPixmapItem *backgroundItem = new QGraphicsPixmapItem( QPixmap::fromImage(backgroundImg));
    scene->addItem(backgroundItem);

    if(controlViewList == NULL)
        return;

    for(int i=0; i<controlViewList->length(); i++)
    {
        QGraphicsItem *currentControl = controlViewList->at(i);
        scene->addItem(currentControl);
    }
}
