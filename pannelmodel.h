#ifndef PANNELMODEL_H
#define PANNELMODEL_H

#include <QObject>
#include <QList>
#include <QJsonDocument>
#include <QJsonArray>

#include "control.h"


class PannelModel : public QObject
{
    Q_OBJECT

public:
    explicit PannelModel(QString deviceId = "");
    ~PannelModel();

    void addControl(Control *newControl);
    void loadJson(QJsonValue json);
    void toJson(QJsonObject *json);

    QString getName() const;
    void setName(const QString &value);

    int getWidth() const;
    void setWidth(int newWidth);

    int getHeight() const;
    void setHeight(int newHeight);

    QList<Control *> *getControlList() const;
    void setControlList(QList<Control *> *value);

    QString getBackground() const;
    void setBackground(const QString &value);

    bool getIsStreamDeck() const;

signals:
    void changed(void);

private slots:
    void updateLayout(void);
    void controlChanged(void);

private:
    QList<Control *> *controlList;

    QString name;
    QString background;
    int width;
    int height;
    size_t streamDeckNumber;
    bool isStreamDeck = false;
};

#endif // PANNELMODEL_H
