#ifndef KEYCONFIG_H
#define KEYCONFIG_H

#include <QWidget>

#include "key.h"

namespace Ui {
class KeyConfig;
}

class KeyConfig : public QWidget
{
    Q_OBJECT

public:
    explicit KeyConfig(QWidget *parent = nullptr);
    ~KeyConfig();

    void setModel(Key *model);

private slots:
    void update(void);

    void iconSelected(QString path);

    void on_name_textEdited(const QString &arg1);

    void on_setColor_clicked();

    void on_commande_textChanged();

    void on_clear_clicked();

    void on_selectIcon_clicked();

private:
    Ui::KeyConfig *ui;

    Key *model;
};

#endif // KEYCONFIG_H
