#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QProcess>
#include <QLabel>
#include <QFile>
#include <QtConcurrent/QtConcurrent>
#include <QNetworkInterface>
#include <QMessageBox>
#include <QFileDialog>

#include "key.h"
#include "configuration.h"
#include "qrcodewidget.h"
#include "streamdeckpp/streamdeckpp.hh"


/**
 * @brief MainWindow constructor
 * @param parent parent widget
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->ui->keyConfig->hide();
    this->ui->selectKey->show();

    this->socket = new JS_Qt_Websocket(APP_NAME, SOCKET_PORT);

    connect(this->socket, &JS_Qt_Websocket::receivedMessage, this, &MainWindow::receivedMessage);

    connect(this->ui->pannelView, &PannelView::selectedControlChanged, this, &MainWindow::controlSelected);

    devicePannelList = new QMap<QString, QList<Pannel *>*>();

    //Creates the contextual menu of the tray icon
    trayIcon = NULL;
    trayIconMenu = NULL;
    showAction = NULL;
    quitAction = NULL;
    createTrayIcon();

    //To hide as soon as possible the app once created we do hide in the main.cpp

    loadJson();

    listDevices();
}

/**
 * @brief MainWindow destructor
 */
MainWindow::~MainWindow()
{
    //socket->close();
    delete ui;
}

void MainWindow::listDevices(void)
{
    streamdeck::context ctx;

    for (size_t i = 0; i < ctx.size(); ++i)
    {
        if (!ctx[i]->connected())
        {
            std::cout << "cannot open device " << 0 << std::endl;
            continue;
        }

        std::cout << "Set image" << std::endl;
        ctx[i]->set_key_image(1, "/home/karlito/.config/kontroldeck/kontroldeck.png");

        // Use the unique ID from the stream deck to remember them and differentiate them
        QString deckName("StreamDeck ");
        deckName += QString::fromStdString(ctx[i]->get_serial_number());

        ui->deviceList->addItem(deckName);
    }

    refreshPannelList();
}

/**
 * @brief Buids a pannel based on a reference file
 * @return A QJsonArray containing the default pannel
 */
QJsonArray MainWindow::buildDefaultPannel(void)
{
    QString defaultFileName = CONFIG_FOLDER + "/default.json";

    QFile defaultConfig(defaultFileName);

    if(!defaultConfig.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // If no default config then we create a default empty pannel
        QJsonArray emptyPannel;
        emptyPannel.append(QJsonValue());
        return emptyPannel;
    }

    QJsonDocument json = QJsonDocument::fromJson(defaultConfig.readAll());

    QJsonArray pannels = json["pannels"].toArray();

    return pannels;
}

/**
 * @brief Triggered when a pannel has been selected (in the pannel on the left)
 * @param pannel Selected pannel
 */
void MainWindow::pannelSelected(Pannel *pannel)
{
    this->ui->pannelConfig->setModel(pannel->getModel());
    this->ui->pannelView->setModel(pannel->getModel());
}

/**
 * @brief When a control as been selected
 * Currently only used in the case where the user clicks on a key/button in the graphical representation of the pannel
 *
 * @param control Selected control
 */
void MainWindow::controlSelected(Control *control)
{
    this->ui->keyConfig->setModel(dynamic_cast<Key *>(control));

    this->ui->keyConfig->show();
    this->ui->selectKey->hide();
}

/**
 * @brief Triggered when a remote (application) sends us a message/request
 * @param command Command string received
 * @param data data attached with the command
 */
void MainWindow::receivedMessage(const QString &command, QJsonValue data)
{
    if(command == "press")
    {
        this->keyPressed(data);
    }
    else if(command == "refresh")
    {
        this->refreshRequest(data);
    }
    else if(command == "imgRequest")
    {
        this->imageRequest(data);
    }
    else
    {
        qDebug() << "Unknown command: " << command;
    }
}

/**
 * @brief Called when a key has been pressed on one of the remote apps
 * @param data contains the informations about wich button was pressed
 */
void MainWindow::keyPressed(QJsonValue data)
{
    uint buttonPressed = data["id"].toInt();

    //Key *keyPresed = dynamic_cast<Key *>(pannelList->at(0)->getModel()->getControlList()->at(buttonPressed));

    //QtConcurrent::run(&Key::runCommand, keyPresed);
}

/**
 * @brief Called when a remote requires a re-sending of the pannel
 * @param data unsused
 */
void MainWindow::refreshRequest(QJsonValue data)
{
    Q_UNUSED(data);

    sendPannel();
}

/**
 * @brief Called when a remote requires an image to be sent
 * @param data contains the path to the image requested
 */
void MainWindow::imageRequest(QJsonValue data)
{
    QString imgPath = data["path"].toString();

    QByteArray ba;
    QBuffer bu(&ba);
    QImage iconImg(CONFIG_FOLDER + "/" + imgPath);

    iconImg.save(&bu, "PNG");
    QString imgBase64 = ba.toBase64();

    QJsonObject responseJson;
    responseJson.insert("path", QJsonValue(imgPath));
    responseJson.insert("binary", QJsonValue(imgBase64));

    qDebug() << "Sending image: " << imgPath;

    if(this->socket != nullptr)
        this->socket->sendCommand("image", responseJson);
}

/**
 * @brief Sends the current pannel to the remote app
 */
void MainWindow::sendPannel(void)
{
    /*Pannel *selectedPanel = pannelList->at(0);

    qDebug() << "Sending pannel";

    QJsonObject pannelJson;
    selectedPanel->getModel()->toJson(&pannelJson);

    QJsonObject controllerJson;
    controllerJson.insert("version", APP_VERSION);
    controllerJson.insert("pannel", pannelJson);

    if(this->socket != nullptr)
        this->socket->sendCommand("controller", controllerJson);*/
}

/**
 * @brief Will close the application completly
 */
void MainWindow::quitPressed(void)
{
    QSettings settings(ORG_NAME, APP_NAME, this);
    QByteArray pannelValue = settings.value("devices").toByteArray();

    // Something changed in the configuration
    if( this->pannelsToJson() != pannelValue)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Save before quitting", "Do you want to save your changes before quitting?",
                                      QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
        if(reply == QMessageBox::Yes)
        {
            on_saveButton_clicked();
            QApplication::quit();
        }
        else if(reply == QMessageBox::No)
        {
            QApplication::quit();
        }
    }
    else
    {
        QApplication::quit();
    }
}

/**
 * @brief Creates the tray icons and actions associated with it
 */
void MainWindow::createTrayIcon(void)
{
    showAction = new QAction(tr("&Show"), this);
    showAction->setCheckable(true);
    connect(showAction, &QAction::triggered, this, &MainWindow::showWindow);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, this, &MainWindow::quitPressed);

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(showAction);
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/image/icon"));

    trayIcon->show();
}

/**
 * @brief Toggles the show/hide status of the mainWindows
 */
void MainWindow::showWindow(void)
{
    if(!showAction->isChecked())
    {
        this->hide();
    }
    else
    {
        this->show();
        this->raise();
    }
}

/**
 * @brief Catches the close event and simply hides the app instead of closing it
 * @param event close event
 */
void MainWindow::closeEvent(QCloseEvent *event)
{
    //we hide the window
    this->hide();
    showAction->setChecked(false);

    //an ignore the close event
    event->ignore();
}

/**
 * @brief Will encode the current pannel into a json file
 * @return the json representation of the pannel
 */
QByteArray MainWindow::pannelsToJson(void)
{
    QJsonObject condifiguration;
    QJsonArray listDevices;

    for(int i=0; i<devicePannelList->size(); i++)
    {
        QList<Pannel *> *pannelsInDevice = devicePannelList->value(devicePannelList->keys().at(i));
        QJsonObject deviceJson;
        QJsonArray listPannel;

        for(int j=0; j<pannelsInDevice->size(); j++)
        {
            QJsonObject pannelsJson;
            pannelsInDevice->at(j)->getModel()->toJson(&pannelsJson);
            listPannel.push_back(pannelsJson);
        }

        deviceJson.insert("id", devicePannelList->keys().at(i));
        deviceJson.insert("pannels", listPannel);

        listDevices.push_back(deviceJson);
    }

    condifiguration.insert("devices", listDevices);

    QJsonDocument json(condifiguration);

    return json.toJson();
}

/**
 * @brief When the user request saving
 */
void MainWindow::on_saveButton_clicked()
{
    QSettings settings(ORG_NAME, APP_NAME, this);
    settings.setValue("devices", this->pannelsToJson());
}

/**
 * @brief MainWindow::copyPath
 * @param src
 * @param dst
 */
void MainWindow::copyPath(QString src, QString dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

/**
 * @brief Will install the default configuration in the home (for linux only)
 * @return The default pannel configuration
 */
QByteArray MainWindow::installDefaulConfig(void)
{
    QByteArray pannelValue;

#ifdef Q_OS_LINUX
    // Copy the default configuration
    copyPath("/etc/kontroldeck/config/", CONFIG_FOLDER+"/");

    qDebug() << "Copied to: " << CONFIG_FOLDER;
    // Read the new configuration
    QSettings settings(ORG_NAME, APP_NAME, this);
    pannelValue = settings.value("devices").toByteArray();
#endif

    return pannelValue;
}

void MainWindow::refreshPannelList(void)
{
    if(ui->deviceList->currentText().isEmpty())
    {
        return;
    }

    QList<Pannel *> *pannelList = devicePannelList->value(ui->deviceList->currentText());

    if(pannelList == nullptr)
    {
        pannelList = new QList<Pannel *>();
        devicePannelList->insert(ui->deviceList->currentText(), pannelList);
    }

    this->ui->pannelList->clear();

    for(int i=0; i<pannelList->length(); i++)
    {
        QListWidgetItem *newItem = new QListWidgetItem();
        auto widget = pannelList->at(i)->getListWidget();

        newItem->setSizeHint(widget->sizeHint());

        this->ui->pannelList->addItem(newItem);
        this->ui->pannelList->setItemWidget(newItem, widget);
    }
}

void MainWindow::addPannel(QJsonValue pannel, QString deviceId)
{
    if(deviceId.isEmpty())
    {
        deviceId = ui->deviceList->currentText();
    }

    Pannel *newPanel = new Pannel(deviceId);
    if(!pannel.isUndefined())
    {
        newPanel->getModel()->loadJson(pannel);
    }

    if(!devicePannelList->keys().contains(deviceId))
    {
        devicePannelList->insert(deviceId, new QList<Pannel *>());
    }

    devicePannelList->value(deviceId)->append(newPanel);

    refreshPannelList();

    connect(newPanel, &Pannel::gotSelected, this, &MainWindow::pannelSelected);
    // If we edit the pannel we send it again to the remotes
    connect(newPanel, &Pannel::changed, this, &MainWindow::sendPannel);
}

/**
 * @brief Reloads the pannel configuration from the saved settings
 */
void MainWindow::loadJson()
{
    QSettings settings(ORG_NAME, APP_NAME, this);
    QByteArray devicesValue = settings.value("devices").toByteArray();

    /*if(devicesValue.isEmpty())
    {
        devicesValue = installDefaulConfig();
    }*/

    QJsonDocument json;
    json = QJsonDocument().fromJson(devicesValue);

    QJsonArray devices = json["devices"].toArray();
    //QJsonArray pannels = json["pannels"].toArray();

    for(int i=0; i<devices.count(); i++)
    {
        QJsonValue deviceConfig = devices[i];
        QString deviceId = deviceConfig["id"].toString();

        QJsonArray pannels = deviceConfig["pannels"].toArray();
        for(int j=0; j<pannels.count(); j++)
        {
            addPannel(pannels[j], deviceId);
        }
    }

    // Automaticaly select the first pannel if available
//    if(pannelList->size() > 0)
//    {
//        pannelSelected(pannelList->at(0));
//    }
}

/**
 * @brief Called when the user clicks on File->Quit
 */
void MainWindow::on_actionQuit_triggered()
{
    this->quitPressed();
}

/**
 * @brief Called when the user clicks on File->Export
 */
void MainWindow::on_actionExport_triggered()
{
    QString destFile = QFileDialog::getSaveFileName(this, "Export to...", "", "*.json");

    if(destFile.isEmpty())
    {
        return;
    }

    QFile file(destFile);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return;
    }

    QTextStream out(&file);
    out << this->pannelsToJson();

    file.close();
}

void MainWindow::on_newPannelButton_clicked()
{
    addPannel();
}


void MainWindow::on_addDeviceButton_clicked()
{
    QDialog qrCodeDialog(this);
    QRCodeWidget qrCode;
    QVBoxLayout layout;

    qrCode.setValue(this->socket->getUrl());
    layout.addWidget(&qrCode);
    qrCodeDialog.setLayout(&layout);

    qrCodeDialog.exec();
}


void MainWindow::on_deviceList_currentTextChanged(const QString &arg1)
{
    refreshPannelList();
}

