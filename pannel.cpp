#include "pannel.h"

#include <QDebug>
#include <QLabel>

/**
 * @brief Abstraction of a Pannel containing both view to display it in the list of pannels and in the configuration view
 */
Pannel::Pannel(QString deviceId)
{
    model = new PannelModel(deviceId);
    connect(this->model, &PannelModel::changed, this, &Pannel::modelChanged);
}

/**
 * @brief Pannel destructor
 */
Pannel::~Pannel()
{

}

/**
 * @brief Called everytime a modification happens in the configuration of the pannel or it's controllers
 */
void Pannel::modelChanged(void)
{
    emit changed();
}

/**
 * @brief Get the model of the pannel (the one with the controllers in it)
 * @return The model associated with this pannel
 */
PannelModel *Pannel::getModel(void)
{
    return model;
}

/**
 * @brief Get the listing widget of this pannel (to show in the list of pannels on the left)
 * @return
 */
QWidget *Pannel::getListWidget(void)
{
    auto listingWidget = new PannelListing(model);

    connect(listingWidget, SIGNAL(selected()), this, SLOT(selected()));

    return listingWidget;
}

/**
 * @brief Called when the pannel is selected in the list on the left
 */
void Pannel::selected(void)
{
    emit gotSelected(this);
}


