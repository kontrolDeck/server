#ifndef ICONSELECT_H
#define ICONSELECT_H

#include <QDialog>
#include <QDir>
#include <QListWidget>

namespace Ui {
class IconSelect;
}

class IconSelect : public QDialog
{
    Q_OBJECT

public:
    explicit IconSelect(const QString &folder, int itemWidth = 100, QWidget *parent = nullptr);
    ~IconSelect();

signals:
    void iconSelected(QString path);

private slots:
    void iconDoubleClicked(QListWidgetItem *item);

    void on_buttonBox_accepted();
    void on_browse_clicked();

private:
    void appendIconFolder(QDir folder);
    void sendSelection(const QString &fileName);

    Ui::IconSelect *ui;

    QDir iconDir;
    QString iconFolder;
    int itemWidth;
};

#endif // ICONSELECT_H
