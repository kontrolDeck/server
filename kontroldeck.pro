QT       += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20 link_pkgconfig

PKGCONFIG += hidapi-libusb Magick++


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    control.cpp \
    iconselect.cpp \
    key.cpp \
    keyconfig.cpp \
    keyview.cpp \
    main.cpp \
    mainwindow.cpp \
    pannel.cpp \
    pannelconfig.cpp \
    pannellisting.cpp \
    pannelmodel.cpp \
    pannelview.cpp \
    QR-Code-generator/cpp/QrCode.cpp \
    js-qt-websocket/Qt/js_qt_websocket.cpp \
    qrcodewidget.cpp \
    streamdeckpp/streamdeckpp.cc

HEADERS += \
    configuration.h \
    control.h \
    iconselect.h \
    key.h \
    keyconfig.h \
    keyview.h \
    mainwindow.h \
    pannel.h \
    pannelconfig.h \
    pannellisting.h \
    pannelmodel.h \
    pannelview.h \
    QR-Code-generator/cpp/QrCode.hpp \
    js-qt-websocket/Qt/js_qt_websocket.h \
    qrcodewidget.h \
    streamdeckpp/streamdeckpp.hh

FORMS += \
    iconselect.ui \
    keyconfig.ui \
    mainwindow.ui \
    pannelconfig.ui \
    pannellisting.ui \
    pannelview.ui \
    qrcode.ui

TRANSLATIONS += \
    kontroldeck_en_US.ts

RC_FILE = kontroldeck.rc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32

INSTALLS += target man link icon16 icon22 icon32 icon48 icon64 icon128 iconscal pixmaps assetbackgrounds asseticons defaultconfig

target.path = /$(DESTDIR)/usr/bin

man.files = doc/kontroldeck.1.gz
man.path = /$(DESTDIR)/usr/share/man/man1

link.files = link/kontroldeck.desktop
link.path = /$(DESTDIR)/usr/share/applications

icon16.files = icon/debian/usr/share/icons/hicolor/16x16/apps/kontroldeck.png
icon16.path = /$(DESTDIR)/usr/share/icons/hicolor/16x16/apps

icon22.files = icon/debian/usr/share/icons/hicolor/22x22/apps/kontroldeck.png
icon22.path = /$(DESTDIR)/usr/share/icons/hicolor/22x22/apps

icon32.files = icon/debian/usr/share/icons/hicolor/32x32/apps/kontroldeck.png
icon32.path = /$(DESTDIR)/usr/share/icons/hicolor/32x32/apps

icon48.files = icon/debian/usr/share/icons/hicolor/48x48/apps/kontroldeck.png
icon48.path = /$(DESTDIR)/usr/share/icons/hicolor/48x48/apps

icon64.files = icon/debian/usr/share/icons/hicolor/64x64/apps/kontroldeck.png
icon64.path = /$(DESTDIR)/usr/share/icons/hicolor/64x64/apps

icon128.files = icon/debian/usr/share/icons/hicolor/128x128/apps/kontroldeck.png
icon128.path = /$(DESTDIR)/usr/share/icons/hicolor/128x128/apps

iconscal.files = icon/debian/usr/share/icons/hicolor/scalable/apps/kontroldeck.svgz
iconscal.path = /$(DESTDIR)/usr/share/icons/hicolor/scalable/apps

pixmaps.files = assets/kontroldeck.png
pixmaps.path = /$(DESTDIR)/usr/share/pixmaps

assetbackgrounds.files = assets/backgrounds/
assetbackgrounds.path = /$(DESTDIR)/etc/kontroldeck/config/

asseticons.files = assets/icons/
asseticons.path = /$(DESTDIR)/etc/kontroldeck/config/

defaultconfig.files = assets/kontroldeck.conf
defaultconfig.path = /$(DESTDIR)/etc/kontroldeck/config/

DISTFILES +=


